let mySound;
let emojis;
let sel;
let dobbeltklik;

function preload() {
  soundFormats('m4a', 'ogg'); //her har jeg angivet den foretrukne filtype. 
  mySound = loadSound('assets/song.m4a');
  emojis = loadModel('assets/tinker.obj', true); //3d model i en tilpasset canvasset størrelse
  dobbeltklik = loadModel ('assets/dobbelt.obj', true);
}

function setup() {
  let cnv = createCanvas(700, 700, WEBGL);
  cnv.doubleClicked(canvasClicked); 
  normalMaterial(); //denne linje giver 3d objektene deres farve
  sel = createSelect(); //select-feltet
  sel.position(300, 550);
  sel.option('hårde tider');
  sel.option('gode tider');
  sel.option('behagelige tider');
  sel.option('svære tider');
  sel.disable('gode tider'); //'disable' gør det umuligt at vælge "gode tider"
  sel.disable('behagelige tider');
}

function canvasClicked() {
  // console.log("test")
  mySound.play(); //denne funktion afspiller sangen 
}
function draw() {
  background(30, 30, 30)

  push(); //push+pop gør det muligt at bruge flere bl.a. 3d objekter sammen
  orbitControl(5); //gør det muligt at flytte på dem (virker bedre uden push+pop)
  translate(0,-100,0); //flytter på modellen
  // rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  scale(1.5);
  // fill(255,204,0);
  // stroke(100,100,0);
  model(emojis);
  pop();

  push();
  translate(0,100,0);
  rotateX(frameCount * 0.02);
  scale(1);
  fill(255,204);
  stroke(255,100);
  model(dobbeltklik);
  pop();
  cursor('assets/cursor.png');
}

