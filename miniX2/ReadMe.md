# MiniX 2

## Tanker & Budskab

Denne opgave gik ud på, at udarbejde 2 geometriske emojis med et politisk/æstetisk bagvedliggende tema. 
Mit valg faldte på nutidens problemer med udfordrende livskvalitet og selve udviklingen af omstridte livstendenser i de seneste 20-40 år, hvor vi som unge bliver mere og mere pressede, idet vi arbejder mere, men har råd til mindre, samt det faktum at vi har så mange muligheder, men alligevel kan så lidt (i det store perspektiv). 

Emojis jeg lavede er to klassiske triste/frustrerede smileys designede i 3D på Tinkercad.com, der hver især skal repræsentere blandingen af håbløshed og afmagtsfølelse, som mange desværre indehaver nu til dags. Idet der allerede findes lignende emojis, ville jeg vha. lyd, interaktion og bevægelse sammensætte en blandet udfoldelse af den mørkere side af realismen, med et stærkt æastetisk aspekt.

Desuden er der også en 3d tekst 'dobbeltklik for audio' - som gør at `surprise`der spiller en trist [sang](https://www.youtube.com/watch?v=XAP8ST5mRgg) en baggrunden. 

For at afspejle følelsen af at leve bl.a. i 2023, har jeg tilføjet et felt med 4 valgmuligheder, hvilke fremstår ret aktuelt i dag, taget i betragtning de seneste negative og deprimerende begivenheder, som : `krig, krise, protester, virus.. osv.`. 

Selve emojis roterer omkring y-aksen, hvorimod teksten roterer omkring x-aksen (her kan man sikkert finde på et smart, delvist skjult budskab, eksempelvis det at verden konstant bevæger sig 'fremad', men mange af samfundets aspekter bevæger sig en helt anden retning - a la lidt på tværs / rebelsk. Dog ville jeg efterlade denne del til tilskueren/brugeren). 
Der blev ligeledes implementeret en aubergine-emoji i stedet for systemets standarde musepil. 

## Decoding

Den visuelle repræsentation af min kode består af en dropdown-menu og to 3D modeller (emojis + tekst), der hver især er tildelt egen variabel. 

Koden for menuen ser således ud:

`createSelect()`

Dropdown menu indeholder 4 valgmuligheder, hvor 2 ud af 4 ikke kan vælges/trykkes på. Dette er gjort således:

Først opretter man selve valgmuligheden - `option()` og så  'deaktiverer' man den med `disable()`.

_______________________________


For at gøre koden endnu mere 'livlig', har jeg valgt at tilføje en sang i baggrunden, der kan aktiverer vha. `canvasClicked()`. Sangen har jeg selv valgt, da jeg synes at den passede godt til den overordnede visuelle stemning.
Noget jeg ikke fik færdiggjort, er funktionen hvor koden tjekker om sangen er i gang med at spille. Lige nu kan man blive ved med at dobbeltklikke canvasset og få sangen til at spille 'over sig selv' igen og igen.  

_______________________________

Jeg har lært en masse i processen, bl.a. hvordan man ved hjælp af syntaksene `push`og `pop` kan tilføje flere 3d elementer i en kode, samt hvordan man ved hjælp af `translate`kan flytte dem rundt på canvasset. 

Desuden, er jeg blevet introduceret til en masse nye 3D syntakser, såsom: 

`orbitControl(5);` = der gør det muligt at trække i 3D modellen, hvor værdien i parenteser definerer 'følsemhed'. 


også er der 2 forskellige 'rotate' funktioner, hhv. om X- og Y akser som jeg har anvendt:

`rotateX(frameCount * 0.02);`

`rotateY(frameCount * 0.01);` 

Værdierne i parenteser er rotationshastighed. 

Og endeligt har jeg lært en rigtig brugbar syntakse, der hjælper med at ændre på 3D modellens størrelsen :

`scale(1.5);`. 

_______________________________

I øvrigt, stødte jeg ind i `normalMaterial();` undervejs, hvilket jeg valgte at afprøve for sjovs skyld. Det viste sig at være en syntax der giver modellen en blanding af RGB farver, hvilket er ret universelt, hvis man ikke vil begrænses til nogle enkelte farver. 

Jeg har alt i alt fundet ud af, at der er rigtig meget i p5js verden jeg ikke ved endnu og det glæder jeg mig til at ændre på.



[Sultans miniX 2](https://sultandd.gitlab.io/sltn/miniX2/index.html)
[kode](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX2/emoji.js)


![](https://sultandd.gitlab.io/sltn/miniX2/screenshot.minix2.png)


