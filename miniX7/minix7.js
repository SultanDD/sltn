let video;
let tracker;
let images = [];
let imgIndex = 0;
let imgSelector;
let somebody;
let button;


// henter billederne
function preload() {
  images.push(loadImage("assets/happy.png"));
  images.push(loadImage("assets/confused.png"));
  images.push(loadImage("assets/mad.png"));
  images.push(loadImage("assets/sketchy.png"));
  images.push(loadImage("assets/scared.png"));
  images.push(loadImage("assets/woke.png"));
}

// opstiller canvas og webcam
function setup() {
  createCanvas(640, 480);
  button = createButton('fundamental part of the experience');
  button.position(220,0);
  button.style('background-color', 'red');
  somebody = createAudio('assets/somebody.m4a');

  // opretter dropdown menu
  imgSelector = createSelect();
  imgSelector.position(width/2, height/1.07);
  imgSelector.option("Happy");
  imgSelector.option("Confused");
  imgSelector.option("Mad");
  imgSelector.option("Sketchy");
  imgSelector.option("Scared");
  imgSelector.option("Woke");
  imgSelector.changed(changeImage);

  // starter webcam
  video = createCapture(VIDEO);
  video.size(width, height);
  // video.hide();

  // aktiverer clmtracker
  tracker = new clm.tracker();
  tracker.init();
  tracker.start(video.elt);
}

function mouseClicked() {
  if (mouseX >= button.position().x && mouseX <= button.position().x + button.width &&
    mouseY >= button.position().y && mouseY <= button.position().y + button.height) {

    somebody.play();
  }
}

// skifter billedet baseret på valg i dropdown menu
function changeImage() {
  let imgName = imgSelector.value();
  if (imgName === "Happy") {
    imgIndex = 0;
  } else if (imgName === "Confused") {
    imgIndex = 1;
  } else if (imgName === "Mad") {
    imgIndex = 2;
  } else if (imgName === "Sketchy") {
    imgIndex = 3;
} else if (imgName === "Scared") {
  imgIndex = 4;
} else if (imgName === "Woke") {
  imgIndex = 5;
  }  
}

// tilpasser billeder ansigtet
function draw() {
  background(255);

  // Tilføjer teksten
  textAlign(CENTER);
  textSize(24);
  fill(100);
  text("Which Shrek are you today?", width/2, 430);

  let positions = tracker.getCurrentPosition();
  if (positions.length > 0) {
    let top = positions[33];
    let bottom = positions[7];
    let distance = dist(top[0], top[1], bottom[0], bottom[1]);

    //  tegner det valgte billede over ansigtet
    let imgWidth = distance * 1.8;
    let imgHeight = imgWidth * (images[imgIndex].height / images[imgIndex].width);
    let imgX = top[0] - imgWidth / 2;
    let imgY = top[1] - imgHeight / 2;

    image(images[imgIndex], imgX, imgY, imgWidth, imgHeight);
  }
}

