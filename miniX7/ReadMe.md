[RunME](https://SultanDD.gitlab.io/sltn/miniX7/index.html)

[kode](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX7/minix7.js)


# Fundamentet til min miniX7
Jeg har valgt at arbejde videre, fuldføre min initiale tanke og færdiggøre min **miniX4**, da jeg aldrig nåede at blive færdig med den tilbage i tiden og var af den grund rigtig frustreret.
Koden forestillede sig at være et simpel, ensfarvet canvas, hvor clmtrackeren kørte i baggrunden og afsporede ens ansigt. 

## Min miniX4 [link til miniX4](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX4/minix4.js)
Den oprindelige miniX4 skulle være præcist som min miniX7 endte med at blive, dog stødte jeg ind i diverse problemer undervejs, hvilket gjorde at jeg måtte aflevere opgaven uden at nå mine mål. 

Resultatet blev et canvas opdelt i to felter: 
1) den øverste var `clmtracker` der sporede ansigtet, dækkede baggrunden og lagde Shreks ansigt oveni.
2) den nederste var det originale feed fra ens webcam, for at kunne sammenligne ens bevægelse med Shreks ansigt. 

Som der blev nævnt i feedbacks til opgaven, fremstod koden underholdende, men menings- og budskabsløs inden for opgavens kontekst - dette kan begrundes med det faktum, at jeg aldrig nåede at komme i mål med min idé. 

Idéen gik ud på, at afspejle nutidens tendens til bl.a. at bruge diverse filtre og masker til at dække sit ansigt med, hvilket ofte resulterede i at man måske tog afstand til sit egentlige jeg og den måde man i virkeligheden så ud/var. Ikke nok med at man forvrængede sit udseende, gik det også ofte ud over ens humør og 'energi', da man ofte var tilbøjelig til at 'lave ansigter' uden rigtigt at føle den skabte vibe, hvilket resulterede i at man igen tog afstand til den tilstand man var i. 

## miniX7
I min miniX7 valgte jeg at være endnu mere stædig og gik videre med udarbejdelsen af min primære tanke. 
En af de større detaljer som det lykkedes mig at få løst, er dropdown menuen med de forskellige facemasks man kan vælge imellem. I den initiale kode, kunne den både ses og interageres med, dog uden en virkning. På en eller anden måde var der ikke skabt 'forbindelse' mellem menuen og resten af koden. 
Til min overraskelse virkede koden næsten med det samme, hvor dropdown menuen rent faktisk gjorde det muligt at skifte mellem Shreks ansigter, som i øvrigt også trackede ens bevægelser. 

## MiniX7 - decoding

Først og fremmest har jeg opstillet de nødvendige variabler: 

`let video;` = web-kameraet

`let tracker;` = clmtracker

`let images = [];` = array for face-masks

`let imgIndex = 0;` = index for antallet af billeder, start på 0 

`let imgSelector;` = dropdown menu

`let somebody;` = sangen

`let button;` = knappen til at afspille sangen

_________________________________________________________________

`imgSelector = createSelect();` = opretter en valgbar menu, og 

`imgSelector.changed(changeImage);` tildeler handlingen en virkning, altså det at billedet skifter sig. 


____________________________

Sangen blev loadet vha. 'createAudio' funktionen:

`somebody = createAudio('assets/somebody.m4a');`

____________________________

Det eneste der ikke fungerede ordentligt i starten, var knappen til at afspille sangen. Denne lille del syntes jeg ville passe rigtigt godt til temaet, hvorfor jeg også valgte at have den med. Den endte med at se således ud:

`createButton('fundamental part of the experience');`


Jeg ved ikke præcist hvorfor knappen drillede i starten, men ved til gengæld hvad hjalp med at løse det. 
I stedet for at danne sammenhæng mellem knappen og sangen, har jeg brugt `mouseClicked()` funktionen, hvilket så således ud: 

`function mouseClicked() {`

`if (mouseX >= button.position().x && mouseX <= button.position().x + button.width &&`

`mouseY >= button.position().y && mouseY <= button.position().y + button.height) {`

`somebody.play();`

Løsningen gik ud på, at angive x-og-y postionen af knappen ind under 'mouseClicked()' funktionen, samt angive selve handlingen `somebody.play`.  

![](https://SultanDD.gitlab.io/sltn/miniX7/screenshot.png)



