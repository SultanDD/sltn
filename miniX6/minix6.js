let alienSize = {
    w:120,
    h:120,
  };
  let alien;
  let alienSpeed = 20;
  let pacPosY;
  let mini_height;
  let min_astronaut = 3;  //min astronaut on the screen
  let astronaut = [];
  let score = 0, lose = 0;
  let keyColor = 200;
  let stars = [];
  let numStars = 50; //antal stjerner
  let moveUp = false;
  let moveDown = false;

  function preload(){
    alien = loadImage("assets/alien.gif");
  }
  function setup() {
    createCanvas(windowWidth/1.1, windowHeight/1.1);
    pacPosY = height/2;
    mini_height = height/2;
    numStars = windowWidth/0.5; //stjernernes 'density' ift. skærmen
    for(let i = 0; i < numStars; i++)
    stars.push(new Star(random(width), random(height), random(1,3), random(1,3)));
  }
  function draw() {
    background(0);
    updateStars();
    fill(keyColor, 255);
    displayScore();
    checkAstronautNum(); //available astronaut
    showAstronaut();
    image(alien, 0, pacPosY, alienSize.w, alienSize.h);
    if (moveUp) {
      pacPosY -= alienSpeed; //samme som nævnt tidligere, bare med Y-aksen
    } else if (moveDown) {
      pacPosY += alienSpeed; //samme 
    }
    checkEating(); //scoring
    checkResult();
  }
  function checkAstronautNum() {
    if (astronaut.length < min_astronaut) {
      astronaut.push(new Astronaut());
    }
  }
  function showAstronaut(){
    for (let i = 0; i <astronaut.length; i++) {
      astronaut[i].move();
      astronaut[i].show();
    }
  }
  function checkEating() {
    //calculate the distance between each astronaut
    for (let i = 0; i < astronaut.length; i++) {
      let d = int(
        dist(alienSize.w/2, pacPosY+alienSize.h/2,
          astronaut[i].pos.x, astronaut[i].pos.y)
        );
      if (d < alienSize.w/2.5) { //close enough as if eating the astronaut
        score++;
        astronaut.splice(i,1);
      }else if (astronaut[i].pos.x < 3) { //alien missed the astronaut
        lose++;
        astronaut.splice(i,1);
      }
    }
  }
  function displayScore() {
      fill(keyColor, 160);
      textSize(17);
      text('You have eaten '+ score + " astronauts", 10, height*0.9);
      text('You have missed ' + lose + " astronauts", 10, height*0.85);
      fill(keyColor,255);
      text('Use ↑ ↓ to move and EAT the astronauts',
      10, height*0.8);
  }
  function checkResult() {
    if (lose > score && lose > 2) {
      fill(keyColor, 255);
      textSize(26);
      text("Too Many Astronauts missed...It's over", width/3, height/1.4);
      noLoop();
    }
  }
  function keyPressed() {
    if (keyCode === UP_ARROW) {
      moveUp = true;
    } else if (keyCode === DOWN_ARROW) {
      moveDown=true;
    }
  }

  function keyReleased() {
    if (keyCode === UP_ARROW) {
      moveUp = false;
    } else if (keyCode === DOWN_ARROW) {
      moveDown = false;
    }
    }
    function updateStars() {
      for(let i = 0; i < stars.length; i++) {
        stars[i].move();
        stars[i].update();
      }
    }
    
    class Star {
      constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.speed = 0.1;
      }
    
      move() {
        this.y += this.speed; //gør at stjernerne bevæger sig
      }
    
      update() {
        fill(255, random(100, 200));
        ellipse(this.x, this.y, this.w, this.h);
        if(this.y > height) { 
          this.y = random(-100, -10);
          this.x = random(width);
        }
      }
    }
