[RunMe](https://sultandd.gitlab.io/sltn/miniX6/index.html)    Hvis ikke den virker med det samme, giv den lidt tid og evt. reload siden :)

[CODE: astronaut](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX6/astronaut.js)

[CODE: canvas, stjerner og rumvæsen](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX6/minix6.js)



## Inspiration
Jeg har lånt koden til baggrunden fra et af de vedhæftede eksempler fra bogen (For additional inspiration): 
"Eat Food Not Bombs (with source code) by Benjamin Grosser (2019), https://editor.p5js.org/bengrosser/full/Ml3Nj2X6w.", da jeg synes at baggrunden så helt magisk ud, med de små blinkende stjerner, men kunne samtidig indse, at jeg endnu ikke selv ville kunne lave noget i den stil. 

Desuden endte jeg med at bruge koden med Pacman der spiser tofu fra bogen, da den kode jeg selv prøvede at bygge fra bunden, kom aldrig til at fungere som den skulle (i mit hoved). 

### the Struggle
I første omgang lavede jeg et canvas med de [blinkende stjerner](https://editor.p5js.org/bengrosser/full/Ml3Nj2X6w). Dernæst fandt jeg gif-billeder af rumvæsnet og astronauten, hvor jeg til at starte med, satte astronauten ind og fik den til at kunne blive flyttet på ved hjælp af ←, →, ↑, ↓. 
Det at man ikke kunne trykke-og-holde tasten for at bevæge sig, syntes jeg var irriterende, derfor fandt jeg ud af hvordan man kunne implementere denne funktion og fik den til at virke vha. google søgning.



# Koden

Ligesom i reverse-coding af Pacman spillet, har jeg lavet 2 .js filer til koden: navnligt astronaut.js og minix6.js. 

Koden til astronauten, indeholder `class Astronaut{}` funktionen, hvor der bl.a. angives hastighed, placering og størrelse (speed, pos, size). 

Hele koden har jeg lavet lidt om på, hvor jeg valgte at bruge gif-billeder for at danne mere bevægelse og dynamik. Ellers, er den fuldstændig magen til koden for tofu fra Pacman. 

Koden til resten er stortset identisk til den fra Pacman, dog har jeg stadig foretaget nogle ændringer for at tilpasse den min idé.

Blandt andet har jeg tilføjet `keyPressed` + `keyReleased` kombi, så man ikke behøver at trykke flere gange for at bevæge sig. 

I forbindelse med det, har jeg angivet følgende variabler: 

`let moveLeft = false;`
`let moveRight = false;`
`let moveUp = false;`
`let moveDown = false;`

Syntax for dette ser således ud:

 `function keyPressed() {`

  `if (keyCode === UP_ARROW) {`

  `moveUp = true;`

  `} else if (keyCode === DOWN_ARROW) {`

  `moveDown=true;`


Sammen med


`function keyReleased() {`

  `if (keyCode === UP_ARROW) {`
  
  `moveUp = false;`
  
  `} else if (keyCode === DOWN_ARROW) {`
  
  `moveDown = false;`

hvor karakteren bliver ved med at bevæge sig i den ønskede retning, så længe knappen holdes ned.

Desuden har jeg generelt leget med størrelser og hastigheder, plus ændret tekst og baggrunden til stjerner (ikke selv skrevet, men ændrede den)


I min `function setup() {` har jeg været nødt til, at gøre canvasset lidt mindre end skærmen, hvilket kan ses her
 `createCanvas(windowWidth/1.1, windowHeight/1.1);` 
 Dette gjorde jeg, da jeg under processen oplevede at canvassets kanter gik over billedets/skærmens størrelse, hvilket gjorde at browseren ikke registrerede ←, →, ↑, ↓ tasterne rigtigt, men flyttede på selve canvas.  



## Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

På dette punkt giver mit spil ligeså meget mening som Pacman der spiser Tofu. 
Dog, kunne man naturligvis tildele idéen et par dybe antydninger, a lá rummet er uendeligt, og man ved aldrig hvad der gemmer sig i det. Eller, truslen om at rumvæsner på et eller andet tidspunkt bliver trætte af os og vores tendens til at nedslide jorden, og kommer for at 'spise' os. 

Alt i alt, synes jeg at de forskellige objekter hænger godt sammen. Navnligt, at det er rumvæsner der 'spiser' kosmonauter, og ikke omvendt. Dette kan argumenteres med at vi, som mennesker, har i mange årtier længtes efter undersøgelsen af rummet. Paradoksalt, ved man hverken om der findes andre civilisationer der gemmer sig fra os, underudviklede civilisationer der ikke er kommet langt nok til at kunne kommunikere eller at vi muligvis selv er rumvæsner der har besat jorden. 
Det uendelige univers med blinkende stjerner forestiller den magiske udsigt de fleste af os kan besigtige hver dag. Budskabet går således ud på tanken, om vi måske først burde undersøge jorden ordentligt? bl.a havbunde og eventuelt også menneskekroppen/hjerne. 

Buckle up, feed your brain and toe the line




![](https://SultanDD.gitlab.io/sltn/miniX6/screenshot.png)
