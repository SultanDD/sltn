class Astronaut {
  constructor() {
    this.speed = floor(random(5, 10));
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(60, 120));

    // astronaut GIF
    this.img = loadImage('assets/cosmo.gif');
  }

  move() {
    this.pos.x -= this.speed;
  }

  show() {
    push();
    translate(this.pos.x, this.pos.y);
    imageMode(CENTER);
    image(this.img, 0, 0, this.size, this.size);  
    pop();
  }
}
