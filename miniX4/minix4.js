let video;
let tracker;
let img;

// henter billedet
function preload() {
  img = loadImage("assets/image1.png");
}

// opstiller canvas og webcam
function setup() {
  createCanvas(640, 480);
  video = createCapture(VIDEO);
  video.size(width, height);
  //video.hide();

  // aktiverer clmtracker
  tracker = new clm.tracker();
  tracker.init();
  tracker.start(video.elt);
}

// tilpasser billede ansigtet
function draw() {
  background(255);
  let positions = tracker.getCurrentPosition();
  if (positions.length > 0) {
    let top = positions[33];
    let bottom = positions[7];
    let distance = dist(top[0], top[1], bottom[0], bottom[1]);

//  tegner billedet over ansigtet
    let imgWidth = distance * 1.8;
    let imgHeight = imgWidth * (img.height / img.width);
    let imgX = top[0] - imgWidth / 2;
    let imgY = top[1] - imgHeight / 2;

    image(img, imgX, imgY, imgWidth, imgHeight);
  }
}
