[link til minix4](https://sultandd.gitlab.io/sltn/miniX4/index.html) (kræver at webcammet virker)
[link til selve koden](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX4/minix4.js)

# Shrek
### Min primære tanke bag miniX4
Indledningsvist, ville jeg prøve at frembringe en kode, som ville, ved hjælp af `clmtracker` følge ens ansigt via ens webcam, men kun vise den i form af tegnede øjne, næse og mund oven på en plain background. Altså, kun bruge ens webcam til at tracke ansigtet, men ikke vise selve webcam-feed. 
Dette virkede, men rigtigt dårligt. Derfor gik jeg over til en anden idé. 

Jeg huskede alle de apps jeg har prøvet at bruge, hvor man bl.a. kunne 'erstatte' sit ansigt med deepfake / facemask, som ville følge ens bevægelse og motorik ret præcist. Dette tænkte jeg, ville være en del nemmere at gennemføre. 

Min idé gik ud på, at lade brugeren vælge mellem 5-7 forskellige ansigter i en dropdown menu, som så ville fungere ligesom en facemask. 
For ikke at lægge en dybere mening i min minix4, men bare at gøre den underholdende, har jeg valgt at bruge Shreks ansigt - vores yndlings-, yderst adorable karakter. 

Efter at have brugt flere timer på at skrive og rette i koden, fandt jeg ud af at den ikke virkede (ordentligt). 

Billedet frøs hver gang man prøvede at skifte 'facemask'. Ellers virkede den okay, dog uden mit primære mål - at kunne skifte ansigtsmasker. 

Efter endnu et par timer med omskrivninger, rettelser og undersøgelser af koden, ~~gav jeg op~~ valgte jeg at vente med at gennemføre hele idéen, og afleverede den 'simple' løsning.  

### Hvad går min miniX4 ud på?
Jeg har valgt at inkludere webcam vha clmtracker, som følger ansigtet, lægger en Shrek maske ovenpå og fjerner baggrunden.
Idet der intet er i baggrunden, danner facemasken 'trail' effekten, hvor man stadig kan se nogle frames, der ligesom følger ens bevægelse.  

### Koden
Først og fremmest, angiver jeg variablene til umiddelbart selve webcam, facetracker og positions, hvilket er afstanden mellem forskellige ansigtsdele. 

`let webcam;
let tracker;
let positions;`

Så preloader jeg et png billede af Shrek, som jeg efterfølgende vil implementere. 
`function preload() {
  // henter billedet
  img=loadImage('assets/image1.png');
}`

Dernæst, følger 'function setup', hvor jeg kreerer selve canvas og etablerer forbindelsen til ens webcam. Feed'et fra kameraet fjernes med `webcam.hide()` 

`function setup() {
  createCanvas(640, 480);
  webcam = createCapture(VIDEO);
  webcam.size(width, height);
  webcam.hide();`

Aktiverer trackeren vi hentede forleden

  `tracker = new clm.tracker();
  tracker.init(pModel);
  tracker.start(webcam.elt);`
`

Så opdaterer jeg trackeren og tilpasser det toppen og bunden af ansigtet.

  `positions = tracker.getCurrentPosition();
  if (positions !== false) {
    let top = positions[65];
    let bottom = positions[5];
    let distance = dist(top[0], top[1], bottom[0], bottom[1]);`


Og så kommer selve png billedet på banen

    `let imgWidth = distance * 1.8;
    let imgHeight = imgWidth * (img.height / img.width);
    let imgX = top[0] - imgWidth / 2;
    let imgY = top[1] - imgHeight / 2;
    image(img, imgX, imgY, imgWidth, imgHeight);`












![screenshot](http://sultandd.gitlab.io/sltn/miniX4/screenshot.png)
