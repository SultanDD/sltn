[RunME](https://sultandd.gitlab.io/sltn/miniX9/index.html)



![ScreenShot](https://sultandd.gitlab.io/sltn/miniX9/screenshotminix9.png)


# MiniX9 E-lit (Gruppe)

For this MiniX we have created a program that highlights the cultural aspects and differences in our group. Since our group consists of members of different cultural backgrounds, such as South-Indian, Eastern-European and Central-Asian, we’ve decided to take advantage of this and create a project which represents our mother-tongues. Our project focuses on the diversity of languages by portraying swear words. As people, we usually connect swear words to a way of releasing frustration while it also being a  way of expressing oneself.  Furthermore one’s vocabulary highlights the way one’s language is influenced by the culture. Swear words are different depending on the language and they have different emotional and cultural impact but they awaken similar emotions such as excitement, frustration and so on

*MERE PÅ VEJ*
