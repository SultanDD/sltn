class pluspoint {
  constructor() {
    this.speed = floor(random(6, 12));
    this.pos = new createVector(random(0, width), -100);
    this.size = floor(random(60, 100));

    // loader billederne
    this.imgs = [];
    this.imgs.push(loadImage('assets/good.png'));
    this.imgs.push(loadImage('assets/good3.png'));

    // vælger et billede på tilfældig vis
    this.img = random(this.imgs);
  }

  move() {
    this.pos.y += this.speed;
  }

  show() {
    push();
    translate(this.pos.x, this.pos.y);
    imageMode(CENTER);
    image(this.img, 0, 0, this.size, this.size);  
    pop();
  }
}

class minuspoint {
  constructor() {
    this.speed = floor(random(6, 12));
    this.pos = new createVector(random(0, width), -100);
    this.size = floor(random(70, 150));
    this.imgs = [];
    this.imgs.push(loadImage('assets/bad.png'));
    this.imgs.push(loadImage('assets/bad3.png'));

    this.img = random(this.imgs);
  }

  move() {
    this.pos.y += this.speed;
  }

  show() {
    push();
    translate(this.pos.x, this.pos.y);
    imageMode(CENTER);
    image(this.img, 0, 0, this.size, this.size);  
    pop();
  }
}
