let gameState = "start"; // laver en 'startside' 
let startbackground;
let cursorImg; // angiver billedet for musepil

let showMessage = false;

let character; //hovedkarakteren
let characterX = 580; // angiver x-koordinaterne for karakteren, ca. midten af canvasset
let characterY = 550; // angiver y-koordinaterne for karakteren, så den er placeret i bunden
let characterSpeed = 15; // karakterens hastighed

//variabler for bevægelser. Det er ikke nødvendigt at tilskrive værdien 'false' til variablene, men god idé for at undgå bugs.  
let moveLeft = false;
let moveRight = false;

let pluspoints = []; // 
let minuspoints = []; // 
let score = 0;



function preload(){
  startbackground = loadImage('assets/stabackgr1.png');
  character = loadImage('assets/character.png'); // loader billedet af karakteren
  cursorImg = loadImage('assets/hammer.png'); // loader billedet der skal erstatte den klassiske musepil 
}

function setup() {
  createCanvas(1440, 800); 
  noCursor(); // skjuler den klassiske musepil

  for (let i = 0; i < 5; i++) {
    pluspoints.push(new pluspoint());
    minuspoints.push(new minuspoint());
  }
}


