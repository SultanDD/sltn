function draw() {
    // opstiller startskærmen
    
    if (gameState === "start") {
      background(startbackground);
      textSize(40);
      textAlign(CENTER, CENTER);
      rectMode(CENTER);
      fill(255,255,0);
      rect(width/2, height/2 + 40, 100, 50);
      fill(255,0,0);
      text("Start", width/2, height/2 + 40);
      image(cursorImg, mouseX, mouseY, 35, 45);
  
      // opstiller gameplayet
  
    } else if (gameState === "playing") {
      if (score < 100) {
        background(100);
      } else if (score >= 100) {
        background(140,10,10);
      }
      if (score >= 200) {
        background(200,40,40);
      }
    if (score >= 300) {
      background(250,60,60);
    }
    if (score <= -100) {
      background(60);
    }
    if (score <= -200) {
      background(40);
    }
    if (score <= -300) {
      background(0);
    }
 

    image(character, characterX, characterY, character.width * 0.4, character.height * 0.4); //width og height halverer størrelsen på karakteren
  
      // angiver parametre ved kollisionen med 'minuspoint' objekter
      for (let i = 0; i < minuspoints.length; i++) {
        let d = dist(characterX + character.width * 0.25, characterY, minuspoints[i].pos.x, minuspoints[i].pos.y);
        if (d < character.height * 0.15 + minuspoints[i].size * 0.1) {
          score-=50; // minus score
          minuspoints.splice(i, 1); // bruger splice-funktionen til at fjerne objektet efter kollisionen 
          break; // bruger denne funktion til at 'standse' for-loopet, så den ikke bliver ved med at lede efter flere kollisioner
        }
      }
  // angiver parametre ved kollisionen med 'pluspoint' objekter
  
      for (let i = 0; i < pluspoints.length; i++) {
        let d = dist(characterX + character.width * 0.25, characterY, pluspoints[i].pos.x, pluspoints[i].pos.y);
        if (d < character.height * 0.15 + pluspoints[i].size * 0.1) {
          score+=50; // plus score
          pluspoints.splice(i, 1);
          break;
        }
      }
    
      // vise 'score' 
      textAlign(RIGHT, TOP);

      // besked dukker op når man når ned på -300 points
if (score === -300 && !showMessage) {
  fill(255,60,60);
  textSize(20);
  text("You have been blacklisted", width / 1.7, height / 2 - 50);
  text("Be careful!", width / 1.7, height / 2 - 20);
}

if (score === -300 && showMessage && mouseIsPressed) {
  showMessage = false;
}
if (score === 300 && !showMessage) {
  fill(255);
  textSize(20);
  text("You have been redlisted", width / 1.7, height / 2 - 50);
  text("the Leader is proud of you!", width / 1.7, height / 2 - 20);
}

if (score === 300 && showMessage && mouseIsPressed) {
  showMessage = false;
}

      textSize(24);
      fill(255);
      text('得分: ' + score, width - 20, 20);
    
  
  if (moveLeft) {
    characterX -= characterSpeed; //'-=' trækker værdien af characterSpeed fra characterX
  } else if (moveRight) {
    characterX += characterSpeed; //'+=' lægger værdien af characterSpeed til characterX
  }
  for (let i = 0; i < pluspoints.length; i++) {
    pluspoints[i].move();
    pluspoints[i].show();
  
    // registrerer at plus-objekterne er gået ud over canvasset
    if (pluspoints[i].pos.y < -pluspoints[i].size) {
      pluspoints.splice(i, 1);
    }
  }
  
  if (frameCount % 60 == 0) { // tilføjer plus-objekter hvert sekund (=hver 60 frames)
    pluspoints.push(new pluspoint());
  }
  
  
  for (let i = 0; i < minuspoints.length; i++) {
    minuspoints[i].move();
    minuspoints[i].show();
  
    if (minuspoints[i].pos.y < -minuspoints[i].size) {
      minuspoints.splice(i, 1);
    }
  }
  
  if (frameCount % 60 == 0) { 
    minuspoints.push(new minuspoint());
  }
  }
  }
  
  //registrerer hændelser ved ←, → tasterne, og ændrer dem til 'true' når de bliver trykt ned
  function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    moveLeft = true;
  } else if (keyCode === RIGHT_ARROW) {
    moveRight = true;
  }
  }
  
  //ændrer dem tilbage til 'false' når tasterne ikke er trykt ned længere
  function keyReleased() {
  if (keyCode === LEFT_ARROW) {
    moveLeft = false;
  } else if (keyCode === RIGHT_ARROW) {
    moveRight = false;
  }
  }
  // registrerer klik på museknappen på start-boksen
  function mousePressed() {
    if (gameState === "start" && mouseX > width/2 - 50 && mouseX < width/2 + 50 && mouseY > height/2 + 15 && mouseY < height/2 + 65) {
      // ændrer tilstanden fra 'start' til 'playing' 
      gameState = "playing";
    }
    if (score === -100 && !showMessage && mouseX > width / 2 - 100 && mouseX < width / 2 + 100 && mouseY > height / 2 - 70 && mouseY < height / 2 - 20) {
      showMessage = true;
    }
  }
