# Hej du dig der!
Jeg har valgt at lave et **animeret javascript** med mulighed for _interaktion_. 
### Tanker behind the Code
Min primære tanke gik ud på, at finde ud af hvordan man kunne skabe en cyberwave-inspireret animation med skiftende farver, der ville køre i loop - dette kunne godt lade sig gøre, men koden var for omfattende til at gennemføre som min første _runme_ og ~~lidt for matematisk, med parametre som sin og cos~~.
### Step-by-step
Derfor gik jeg over til en anden idé, hvor jeg istedet ville bruge en **roterende figur**.
Først og fremmest, opsatte jeg variablen ved hjælp af `let` med den geometriske parameter `detailX`. 
Derefter lavede jeg selve _canvas_, 500x500, i en mild rød/orange farve. Her brugte jeg `createSlider` funktionen jeg fandt i ps.js reference-listen. Den har jeg så indstillet til at være placeret nederst i midten. 

Dermed brugte jeg `function draw()`, hvor jeg så tegnede en roterende _cylinder_, som så blev 'koblet' sammen med `detailX` parametren, så jeg kunne bruge slider til at ændre på cylinderen - hvilket kan ses i koden. 

I den primære cylinder kode, angav man `background` farven inde i selve koden for cylinder, men den flyttede jeg så ~~for sjov~~ ind under koden var _canvas_, hvilket så gjorde at cylinderen har fået en duplikerende delay/shadow effekt. Jeg syntes, at det så fedt ud, så jeg valgte at beholde den. 

Dernæst, prøvede jeg at lege videre med geometrien, hvor jeg forsøgte at skabe et statisk rektangel, med animeret indhold. Her kan man sige at : mission was failed succesfully :D, for selvom det blev til noget helt andet end det jeg ønskede, blev det fedt.  
[link til min minix](https://SultanDD.gitlab.io/sltn/miniX1/index.html)
<details><summary>Mislykket</summary>
Jeg forsøgte også et utal af gange at tilføje en baggrundsmelodi, blandt andet ved hjælp af `loadSound` funktionen, men intet virkede ~~endnu~~. Det skal jeg nok forsøge igen på et tidspunkt. 


![](https://sultandd.gitlab.io/sltn/miniX1/screenshot.png)


