let detailX
function setup() {
  createCanvas(500, 500, WEBGL);
  background(205, 105, 94);
  detailX = createSlider(3, 24, 3);
  detailX.position(200, height - 50);
  detailX.style('width', '90px');
};
function draw() {
  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
  cylinder(60, 200, detailX.value(),1);
  rect(60,200,detailX.value(),1);
}
