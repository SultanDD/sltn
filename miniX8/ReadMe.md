[Abi](https://gitlab.com/Abishana/aestetiskprogrammering)


[Alexandra](https://gitlab.com/leealexa12/aestetiskprogrammering)

# Individuelt
I have chosen to revisit my miniX6, which I regard as the most comprehensive and time-consuming code I have made so far.
![](https://sultandd.gitlab.io/sltn/miniX8/flowchartPersonal.png)



# Gruppe

### First Idea
‘Social Credit’ game - our idea is (obviously) based on the inhumane and unfair side of an East-Asian country, which is also a good example of how communism only works good ‘on paper’. 
The atmosphere and character of the game are provoking and satiric, but without any means to harm or offend anyone. The desired goal of the game is to depict the currently evolving situation in the very country, where the sole ruling national party of people works hard in order to gain full control of the population, which often results in violation of human rights and evokes local and global conflicts.

!![](https://sultandd.gitlab.io/sltn/miniX8/flow.png)


### Second idea
Considering that everything seems to become more digitalized in today’s world, people start relying heavily on technology and the internet in their everyday lives. People expose their personal information, more or less aware of the possible threats and cosequences that can be encountered in the digital world, through platforms such as social media or online banking. We decided through our project to focus on data capturing and the potential threat of hacking, since it is a relevant concern and an actual problem in our society.

Therefore we’ve created an idea which focuses on data capturing and hacking. The idea is an app which is meant to have the exact layout and functions as the Danske Bank mobile app. This app is meant to show the disadvantages of digitalisation and spread awareness about how vulnerable people are in the digital world. This is the reason for the implementation of glitches when an interaction has occurred with one of the features/buttons on the app screen.

!!![](https://sultandd.gitlab.io/sltn/miniX8/flow2.png)

### Questions

#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
Simplifying an algorithmic procedure and programs/codes is difficult since these are complex. They are complex in the sense that they don’t always turn out to be the way we’d intended them to be, they also don’t function according to our desire. Therefore having to phrase such complex processes and codes with simplicity is quite a challenge. 

#### What are the technical challenges facing the two ideas and how are you going to address these?
The first possible challenge of our first idea could be the idea itself. Since it is quite easy to spot the ‘true’ meaning of the objects in the game, it might be hard for some to fully accept the satiric and harmless message of the game and digest the represented  theme.  Hence, from the technical perspective, it could definitely be done in a more implicit way, without missing its’ theme. 
When it comes to technical challenges, it would take a lot of work and thinking-through to turn the first idea into the code, that is, the optimization of the small details, like : 

What exactly do objects (Winnie, Person) look like and how do they move?

How do they interact and what happens when they finally do?

Are the interaction and the outcome ‘smooth’  enough? If not, how to make the code run and generate the objects smoother. 


The technical challenge of the second idea would probably be the implementation of the glitches onto the features. Firstly we’ll have to create glitches which are different from one another. Hereafter we’ll be adding these glitches to the individual features. And at last we’ll have to make sure that the whole program runs smoothly.

#### In which ways are the individual and the group flowcharts you produced useful?
The flowcharts are useful to get a better understanding of the algorithms and different paths behind the code. The flowchart is a simplified run-through of the program. This especially becomes useful when having to introduce the process and meaning of a design to the users, together with having a clear understanding and overview of it as a designer.

