let antalskyer = 20; // angiver antal af skyer
let skyer = []; // laver et array til skyerne
let skybillede; // billede af skyen
let regndraaber = []; // laver et array for regnen 
let maksantalregn = 1000; // maks antal af regndråber

function preload() {
  skybillede = loadImage('assets/cloud.png'); // preloader et png af en tegnet sky 
}

function setup() {
  createCanvas(windowWidth, windowHeight); // laver et canvas der tilpasser sig skærmens dimensioner 
  background(0); // laver en sort baggrund

// genererer skyerne
  for (let i = 0; i < antalskyer; i++) {
    let x = random(width);
    let y = random(height / 6); // 'splitter' canvassets højde, så skyerne dukker op næsten helt øverst
    let size = random(80, 300); // forskellige størrelser 
    skyer.push(new Sky(x, y, size)); // tilføjer 'new Sky' element til 'skyer' array med de angivne x,y,size parametre. 
    //gør det muligt at multiplicere variablens parametre, for at bruge dem senere
  }
}

function draw() {
  background(0); 
  // 'renser' canvasset. 
  // Hvis man fjerner denne linje, får man en sjov animation, hvor regndråberne fylder canvasset indtil det ligner en barkode :)

  // angiver parametre for skyerne

  for (let i = 0; i < antalskyer; i++) {
    skyer[i].display(); // tegner skyerne  
    skyer[i].update(); // opdaterer skyerne
  }

  // tilføjer regndråber til canvasset
  while (regndraaber.length < maksantalregn) { // while-loop sørger for at tilføje regndråber indtil maks antal er nået

    // vælger random koordinater

    let x = random(width); 
    let y = random(height);
    let speed = random(1, 3); //angiver maks hastighedsintervaler
    regndraaber.push(new regndraaber2(x, y, speed)); // tilføjer 'regndraaber2' til arrayet. Gør det muligt at multiplicere variablens parametre, for at bruge dem senere
  }

  // tegner og opdaterer regndråber

  for (let i = 0; i < regndraaber.length; i++) {
    regndraaber[i].display();
    regndraaber[i].update();
  }
}

class Sky { // class funktionen gentager tidligere variabels parametre, hvor jeg kan modificere dem. Dette er gengivelse af arrayet med skyerne. 
  constructor(x, y, size) { //constructor er en del af 'class' funktionen, der ligesom aktiverer de initiale parametre
    this.x = x;
    this.y = y;
    this.size = size;
    this.speed = random(0.001, 0.7); // angiver maks- og min hastighed, der bliver randomiseret
  }

  // bruger billedet af sky
  display() {
    image(skybillede, this.x, this.y, this.size, this.size);
  }

  // angiver parametre for skyerne
  update() {
    this.x += this.speed; 
    if (this.x > width + this.size) {
      this.x = -this.size;
      this.y = random(height/2);
      this.size = random(50, 100); // random størrelse, min og maks
      this.speed = random(0.0001, 0.1); // random hastighed
    }
  }
}

class regndraaber2 { // class funktionen gentager tidligere variabels parametre, hvor jeg kan modificere dem. Dette er gengivelse af arrayet med regndråber. 
  constructor(x, y, speed) {  //constructor er en del af 'class' funktionen, der ligesom aktiverer de initiale parametre
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.length = random(2, 10);
  }

  // tilføjer regndråberne
  display() {
    stroke(255);
    line(this.x, this.y, this.x, this.y + this.length);
  }

  // opdaterer regndråbene
  update() {
    this.y += this.speed;
    if (this.y > height) {
      this.y = random(-height, 0);
      this.speed = random(2, 5);
    }
  }
}
