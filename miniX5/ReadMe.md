[link til RunMe](https://sultandd.gitlab.io/sltn/miniX5/index.html)


[link til koden](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX5/minix5.js)

# "the Rain" 

I min miniX5 har jeg lavet en animation, der simulerer regnvejr og skyer, idet det var det første jeg kom i tanke om, da jeg hørte om auto-generations konceptet. Det er sjældent man analyserer formerne af regndråber og skyer, da det er en rigtigt triviel ting her i Danmark. Men når man endelig tænker over det, så er der ikke så mange andre ting der kunne være mere 'selvgenererende' end regndråber og skyer. Disse to fænomener er altså blandt de få ting vi kan opleve, som nærmest altid kommer i forskellige form og størrelser. 

### Princip - kort 

Koden opretter et tilfældigt antal skyer og regndråber, hvor diverse variabler og metoder bliver brugt til at generere dem forskellige steder på canvasset. Jeg har valgt at bruge en sort baggrund og hvide/grå regndråber og skyer, for at skabe mere kontrast i farverne, hvilket efter min mening har en visuelt æstetisk effekt. 


## Kode

Først og fremmest, har jeg angivet de nødvendige variabler: 



`let antalskyer = 20; // angiver antal af skyer`
 
`let skyer = []; // laver et array til skyerne`

`let skybillede; // billede af skyen`

`let regndraaber = []; // laver et array for regnen`

`let maksantalregn = 1000; // maks antal af regndråbe`



Her har jeg blandt andet defineret antal af skyer, antal af regndråber, opstillet variablen for billedet og skabt arrays til skyer og regndraaber, som jeg længere henne i koden brugte til at automatisere generering af de pågældende genstande. 

________________________

Dernæst har jeg brugt `for` loop funktionen til umiddelbart at generere skyerne og regndråber, hvilket kan ses i linjerne :

`for (let i = 0; i < regndraaber.length; i++) {`

og

`for (let i = 0; i < antalskyer; i++) {` 


hvor jeg blandt andet kunne få dem til at spawne forskellige steder på x og y aksene, have forskellige størrelser og bevæge sig med forskellig hastighed, ved hjælp af funktionen `random()`. 

Eksempel:

`let x = random(width);`

`let y = random(height / 6);` (dukker op random steder i det angivne område af canvasset)

`let size = random(80, 300);` ...og i forskellige størrelser



En af de fedeste ting jeg har lært under processen, er kombinationen af funktionerne `push`,`new`, `class`, `constructor` og `while`, der gør det muligt at spare en masse tid ved at generere loop af et genstand og på en måde kopiere den med nye parametre. 

For eksempel her i denne linje  `skyer.push(new Sky(x, y, size));`
Ved hjælp af `push` overfører jeg `skyer`-arrayets parametre ind i det nye objekt `new Sky`. 

Det nye objekt redigerer jeg så senere hen, ved hjælp af `class` og `constructor` :

`class Sky {`

`Constructor(x, y, size) {`

`this.x = x;`

`this.y = y;`

`this.size = size;`

`this.speed = random(0.001, 0.7);` her randomiserer jeg så hastigheden. 

class funktionen gentager tidligere variabels parametre, hvor jeg kan modificere dem. Dette er gengivelse af arrayet med skyerne. 

Selve billedet af skyen, indhenter og fremviser jeg vha. denne syntax:
`display() {`
`image(skybillede, this.x, this.y, this.size, this.size);`


Her bruger jeg så også `update()`, til at generere og tilføje nye objekter af samme klasse, dog med nye parametre.  

Det samme gælder `regndraaber`, der ligesom `sky`, dupliceres og opdateres.  

Desuden har jeg i denne kode prøvet at bruge loopet [while](https://p5js.org/reference/#/p5/while) 
`while(regndraaber.length < maksantalregn)`, der bliver ved med at tilføje regndråberne indtil det angivne maks antal er nået.

## Selvkørende kode vs. Regler

Selvom koden i princippet er 'selvkørende', hvor den på en måde har en fri vilje til selv at vælge størrelse, hastighed og placering af objekterne, så følger syntakserne stadig de regler og betingelser jeg har angivet, blandt andet:

- det maksimale antal af skyer og regndråber
`let antalskyer = 20`

- andel af skærmen hvor objekterne skal spawne
`let y = random(height / 6)`

- maks og min hastighed, samt maks og min størrelser
`this.speed = random(0.0001, 0.1)`

`let size = random(80, 300)`






![](https://sultandd.gitlab.io/sltn/miniX5/screenshot.png)
