let img;
let progress = 0; //loading bar variablen
let angle = 0; //variablen der styrrer rotationsvinklen 
let diameter = 40; //diameter af throbber
let speed = 2; //hastigheden af throbber
let numEllipses = 8; //størrelsen af throbber
let button;


function preload() {
  img = loadImage('assets/back.png'); //pre-loader baggrundsbilledet
}

function showRestartButton() { //koden til at lave en restart-knap
  button = createButton("Want to live again?");
  button.position(width/2 - 50, height/1.08);
  button.mousePressed(restartSketch); //knappen aktiveres ved at trykke på musen
}


function restartSketch() { //angiver funktionen til at restarte animationen og koden
  window.location.reload();
}

function setup() {
  createCanvas(800, 600);
  imageMode(CORNER); //justerer billedet, så den fylder canvasset, i stedet for at være i midten 
  angleMode(DEGREES);
  noFill();
  stroke(255);
}

function draw() {
  background(200);
  let x = (width - img.width) / 2; // billedets x position
  let y = (height - img.height) / 2; // billedets y position
  image(img, x, y); //placerer billedet ved de angivne koordinater
  fill(0);
  rect(300, 500, 200, 20);
  fill(255);
  rect(300, 500, progress * 2, 20); //angiver placeringen, hastigheden og størrelsen af loading-barren
  progress += 0.1;
  if (progress >= 100) {  //if-statement angiver flere parametre her når loading-bar når op på 100%
    noLoop(); // stopper animationen
    textSize(20);
    fill(255, 0, 0);
    text("you died", width/2.3, height/1.1);
    showRestartButton(); //restart knappen dukker op først når man når op på 100
  }
  for (let i = 0; i < numEllipses; i++) { //her er parametre og selve koden for throbber
    let d = diameter + sin(angle + i * 45) * diameter / 2; //matematisk udtryk, hvor d er regnet ud fra værdier af diameter og angle. 
    let x = width/2 + cos(i * 45) * (diameter + 20); //kalkulerer bredde-positionen af cirklene og lægger +20 pixler oveni.
    let y = height/2 + sin(i * 45) * (diameter + 20); //kalkulerer højde-positionen af cirklene og lægger +20 pixler oveni.
    ellipse(x, y, d, d); //tegner cirklene med de angivne parametre
  }
  angle += speed; //er det samme som angle = angle + speed. Dette lægger værdier af hastighed og vinklen sammen og 'returnerer' dem til angle, hvilket skaber effekten af bevægelse. 
}
