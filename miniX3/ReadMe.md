[link til Sultan's minix3](https://sultandd.gitlab.io/sltn/miniX3/index.html)

[link til kode](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX3/cylinder.sketch.js)

# Preface

Til dagligt, støder man så ofte ind i 'throbbere', at man ikke nødvendigvis lægger mærke til dem længere. Personligt, er jeg gradvist begyndt at opfatte en throbber som en 'accessorisk del' af enhver opstået ventetid når man surfer på nettet. 
Selvom jeg forstår at en throbber i de fleste tilfælde, blot er en animation (GIF eller js), får jeg alligevel associationer om at der er et eller andet der bliver 'loadet' eller analyseret. 

I min miniX3, havde jeg i første omgang en idé om at lave en psykedelisk, farverig og støjfyldt throbber, men valgte efterfølgende at prøve at lægge en dybere mening i min kode, end blot æstetisk. 

# miniX3 - readMe "~~life is like a~~ Throbber"

I min _miniX3_, har jeg forsøgt at afspejle måden hvorpå hverdagen foregår nu til dags for rigtig mange mennesker hele verden rundt, samt at danne et overblik over hvordan livet bliver 'brændt ud' uden at man rigtigt lægger mærke til det - dette er for skabe en reference til effekten af uendelighed som en throbber-animation ofte besidder. 
Throbberen i dette tilfælde kan betragtes som en ond cirkel, hvor selve den repetative bevægelse symboliserer mennesker, der strejfer imellem de velkendte poster, navnlig arbejde, trafik og hjem. 

## Ulemper

Der er naturligvis en del detaljer som går glip ved sådan en generaliserende opstilling. Eksempelvis al den glæde man kan opleve ved såvel små som store ting eks. fødselsdage, helligdage, ferie og andre opnåelser man kan tilegne sig, som at få en hund, fisk eller hamster. 
Dog, kommer det hele frem til et identisk mønster, den onde cirkel, hvor man i det store perspektiv selv kan minde om en hamster, der lever sit liv i burret, uden rigtigt at prøve at finde ud af hvad der ellers findes derude.         



### Visuelt

[inspirationskilde](https://editor.p5js.org/black/sketches/HJbGfpCvM)

På det visuelle punkt, har jeg lavet en, knap så interaktiv, men rig på bevægelse og animation kode. **Baggrundsbilledet** er et screenshot af min pc's baggrundsbillede, som er Apples standard 'flot-udsigt' wallpaper, der illustrerer en landevej der strækker sig igennem nogle flotte bjerge. 
modificeret ved brug af bl.a. emojis af et gammelt og ødelagt hus, bus, trafiklys og en bygning (kontor) + de dertilhørende navne. Jeg valgte at gøre det på denne måde, for at spare tid, da jeg vidste at skabelsen af en throbber ville være ret omfattende og tung proces. 

Der er en animeret loading-bar i bunden, der "styrer" hele molevitten ved hjælp af if-statement `if (progress >= 100)...noLoop();`, idet elementerne i koden standser `noLoop` når barren er fyldt, og genstart-knappen `button = createButton("Want to live again?");` aktiveres og dukker op. 

Restart knappen er sat i gang ved hjælp af funktionen 
`function restartSketch() {`
`window.location.reload();`. 

Hvilket er koblet til restartknappen og aktiveres således : 
`button.mousePressed(restartSketch)`

_________________________




Selve loading-barren, starter ved positionen (300, 500) og har en bredde på `progress * 2`, hvilket gør at den fyldes gradvist op over tid ved at øge variablen `progress` med 0,1 hver gang koden eksekveres. 

## Throbber

Her er den ikke særlig skønne kode for throbberen :

`for (let i = 0; i < numEllipses; i++) {`

  `let d = diameter + sin(angle + i * 45) * diameter / 2;`

  `let x = width/2 + cos(i * 45) * (diameter + 20);`

  `let y = height/2 + sin(i * 45) * (diameter + 20);`

  `ellipse(x, y, d, d);`

  `}`

  `angle += speed;`

Den ser uoverskueligt ud, men jeg har prøvet at forklare den i min .js fil så godt jeg selv kunne. Nogle ting har jeg ikke helt forstået, men man kan se hvad de gør ved at ændre i dem, for at danne en lidt bedre forståelse af hvert enkelt element.





  



![screenshotminix3](https://sultandd.gitlab.io/sltn/miniX3/screenshotminix3.png)


